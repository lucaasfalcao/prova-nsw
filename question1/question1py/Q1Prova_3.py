import Q1Prova_2

#Classificação do imc
def imc_rating(imc):
    if imc<17:
        print('Status correspondente: Muito abaixo do peso.')
    elif 17<=imc<=18.49:
        print('Status correspondente: Abaixo do peso.')
    elif 18.50<=imc<=24.99:
        print('Status correspondente: Peso normal.')
    elif 25<=imc<=29.99:
        print('Status correspondente: Acima do peso.')
    elif 30<=imc<=34.99:
        print('Status correspondente: Obesidade grau I.')
    elif 35<=imc<=39.99:
       print('Status correspondente: Obesidade grau II.')
    elif imc>=40:
        print('Status correspondente: Obesidade grau III.')

imc_rating(Q1Prova_2.imc)