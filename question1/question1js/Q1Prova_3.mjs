import {calculImc} from './Q1Prova_2.mjs';

//Classificação do imc
const imcRating = () =>{
    let imc = calculImc();
    if (imc <= 16.99){
        console.log('Status correspondente: Muito abaixo do peso.')
    }
    else if (imc <= 18.5){
        console.log('Status correspondente: Abaixo do peso.')
    }
    else if (imc <= 24.99){
        console.log('Status correspondente: Peso normal.')
    }
    else if (imc <= 29.99){
        console.log('Status correspondente: Acima do peso.')
    }
    else if (imc <= 34.99){
        console.log('Status correspondente: Obesidade grau I.')
    }
    else if (imc <= 39.99){
        console.log('Status correspondente: Obesidade grau II.')
    }
    else if (imc >= 40){
        console.log('Status correspondente: Obesidade grau III.')
    }
}

imcRating();