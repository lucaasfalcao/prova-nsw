import {userData} from './Q1Prova_1.mjs';

//Cálculo do imc
export const calculImc = () =>{
    let data = userData();
    let imc = data.weight/(data.height*data.height);
    console.log('Valor do IMC:', imc.toFixed(2));
    return imc;
}
