import promptSync from 'prompt-sync';

//Leitura de dados
export const userData = () =>{
    const prompt = promptSync();
    let height = prompt('Digite sua altura (em metros): ');
    let weight = prompt('Digite seu peso (em kg): ');
    console.log();
    return{
        height, weight
    }
}