import promptSync from 'prompt-sync'

//Leitura do inteiro N
export const integerValue = () =>{
    const prompt = promptSync();
    let integer = prompt('Digite um inteiro N: ');
    console.log();
    return integer;
}