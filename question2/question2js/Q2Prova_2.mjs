import { integerValue } from "./Q2Prova_1.mjs";
import promptSync from 'prompt-sync';

//Leitura de até 20 inteiros X e impressão da quantidade de repetições do inteiro N
const integerRepetition = () =>{
    const prompt = promptSync();
    let integer = integerValue();
    console.log('Obs: Caso o número de inteiros X desejado seja alcançado, digite -1.');
    console.log();
    let cont = 0;
    for (let i = 0; 21; i++){
        let x = prompt('Digite o inteiro X' + i + ':')
        if (x == -1){
            break
        }
        if (x == integer){
            cont = cont+1
        }
    }
    console.log()
    console.log('O inteiro', integer, 'apareceu', cont, 'vezes.')
}
    
integerRepetition();