import Q2Prova_1

#Leitura de até 20 inteiros X e impressão da quantidade de repetições do inteiro N
def integer_repetition(integer):
    print('Obs: Caso o número de inteiros X desejado seja alcançado, digite -1.')
    print()
    cont=0
    for i in range(21):
        x = int(input('Digite o inteiro X{}: '.format(i)))
        if x==-1:
            break
        if x==integer:
            cont=cont+1
    print()
    print(f'O inteiro {integer} apareceu {cont} vezes.')

integer_repetition(Q2Prova_1.integer)