import Q3Prova_1

#Leitura dos dados de cada boi
def oxen_data(quantity):
    ox = {}
    fatter = 0
    thinner = 0

    for i in range(quantity):
        ox[f'ox{i+1}'] = []
    
        identification = int(input('Digite o número de identificação do {}° boi: '.format(i+1)))
        ox[f'ox{i+1}'].append(identification)
    
        name = input('Digite o nome do {}° boi: '.format(i+1))
        ox[f'ox{i+1}'].append(name)
    
        weight = float(input('Digite o peso do {}° boi (em kg): '.format(i+1)))
        ox[f'ox{i+1}'].append(weight)
        print()

        if i==0:
            fatter = thinner = ox[f'ox{i+1}'][2]
        else:
            if ox[f'ox{i+1}'][2] > fatter:
                fatter = ox[f'ox{i+1}'][2]
    
            if ox[f'ox{i+1}'][2] < thinner:
                thinner = ox[f'ox{i+1}'][2]

    return ox, fatter, thinner

ox, fatter, thinner = oxen_data(Q3Prova_1.quantity)