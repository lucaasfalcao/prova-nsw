//Impressão do boi mais gordo e do mais magro
const prompt = require('prompt');
const readData = require('./Q3Prova_2')
const f3 = async () =>{
    prompt.start();
    let ox = await readData(prompt);
    ox = ox.map(({ id, name, weight}) => ({ id: parseInt(id, 10), name: name, weight: parseFloat(weight)}));
    ox.sort((x, y) => parseFloat(x.weight) - parseFloat(y.weight));
    const general = ox.length
    console.log(`Boi mais gordo: id: ${ox[general-1].id} | nome: ${ox[general-1].name} | peso: ${ox[general-1].weight} kg`);
    console.log(`Boi mais magro: id: ${ox[0].id} | nome: ${ox[0].name} | peso: ${ox[0].weight} kg`);
}
f3();