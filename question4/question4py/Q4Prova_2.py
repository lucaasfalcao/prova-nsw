import Q4Prova_1
import numpy as np

#Leitura de cada inteiro e criação do array com todos eles
def integer_array(quantity):
    if quantity <= 100000:
        integer_list = []
        for i in range(quantity):
            integer = int(input('Digite o {}° inteiro: '.format(i+1)))
            integer_list.append(integer)
    else:
        print('Erro: quantidade de inteiros acima do permitido.')

    array = np.array(integer_list)
    print()
    return array

array = integer_array(Q4Prova_1.quantity)