import Q4Prova_2, Q4Prova_1
import numpy as np

#Leitura da quantidade de deslocamentos e impressão do array deslocado
def displacements(array):
    displacement_quantity = int(input('Digite a quantidade de deslocamentos a serem feitos no array: '))
    print()

    if 0 <= displacement_quantity <= Q4Prova_1.quantity:
        shifted_array = np.roll(array,-displacement_quantity)
        print('Array com os deslocamentos:')
    
        for j in range(len(shifted_array)):
            print(shifted_array[j])
    else:
        print('Erro: quantidade de deslocamentos não permitida.')
    
displacements(Q4Prova_2.array)