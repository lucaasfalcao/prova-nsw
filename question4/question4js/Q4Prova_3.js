//Leitura da quantidade de deslocamentos
const f3 = async (prompt) =>{
    const displacementQuantity = await prompt.get(['shift']);
    return parseInt(displacementQuantity.shift, 10)
}
module.exports = f3