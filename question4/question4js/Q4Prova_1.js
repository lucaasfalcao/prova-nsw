//Leitura da quantidade de inteiros no array
const f1 = async (prompt) =>{
    const quantityOfIntegers = await prompt.get(['quantity']);
    return parseInt(quantityOfIntegers.quantity, 10);
}
module.exports = f1